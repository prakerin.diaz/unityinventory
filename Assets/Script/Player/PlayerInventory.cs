using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class PlayerInventory : MonoBehaviour
{
    public InventoryObject Inventory;
    public InventoryObject Equipment;
    public Attribute[] attributes;

<<<<<<< Updated upstream
=======
    private Transform boots;
    private Transform armor;
    private Transform helmet;
    private Transform leftHand;
    private Transform rightHand;

    private BoneCombiner boneCombiner;

    public Transform weaponTransform;
    public Transform leftWristTransform;
    public Transform leftHandTransform;

>>>>>>> Stashed changes
    private void Start()
    {
        boneCombiner = new BoneCombiner(gameObject);

        for (int i = 0; i < attributes.Length; i++)
        {
            attributes[i].SetParent(this);
        }
        for (int i = 0; i < Equipment.GetSlots.Length; i++)
        {
            Equipment.GetSlots[i].OnBeforeUpdate += OnBeforeSlotUpdate;
            Equipment.GetSlots[i].OnAfterUpdate += OnAfterSlotUpdate;
        }
    }
    public void OnBeforeSlotUpdate(InventorySlot _slot)
    {
        if (_slot.ItemObject == null)
            return;
        switch (_slot.Parent.Inventory.Type)
        {
            case InterfaceType.Inventory:
                break;
            case InterfaceType.Equipment:
                print(string.Concat("Removed ", _slot.ItemObject, " on ", _slot.Parent.Inventory.Type, ", Allowed Item: ", string.Join(", ", _slot.AllowedItems)));

                for (int i = 0; i < _slot.Item.Buffs.Length; i++)
                {
                    for (int j = 0; j < attributes.Length; j++)
                    {
                        if (attributes[j].type == _slot.Item.Buffs[i].attributes)
                            attributes[j].value.RemoveModifier(_slot.Item.Buffs[i]);
                    }
                }
                if (_slot.ItemObject.characterDisplay != null)
                {
                    switch (_slot.AllowedItems[0])
                    {
                        case ItemType.Weapon:
                            Destroy(rightHand.gameObject);
                            break;
                        case ItemType.SecondWeapon:
                            switch (_slot.ItemObject.Type)
                            {
                                case ItemType.Weapon:
                                    Destroy(leftHand.gameObject);
                                    break;
                                case ItemType.SecondWeapon:
                                    Destroy(leftHand.gameObject);
                                    break;
                                default:
                                    break;
                            }
                            break;
                        case ItemType.Armor:
                            Destroy(armor.gameObject);
                            break;
                        case ItemType.Boots:
                            Destroy(boots.gameObject);
                            break;
                        case ItemType.Helmet:
                            Destroy(helmet.gameObject);
                            break;
                    }
                }
                break;
            default:
                break;
        }
    }
    public void OnAfterSlotUpdate(InventorySlot _slot)
    {
        if (_slot.ItemObject == null)
            return;
        switch (_slot.Parent.Inventory.Type)
        {
            case InterfaceType.Inventory:
                break;
            case InterfaceType.Equipment:
                print(string.Concat("Placed ", _slot.ItemObject, " on ", _slot.Parent.Inventory.Type, ", Allowed Item: ", string.Join(", ", _slot.AllowedItems)));

                for (int i = 0; i < _slot.Item.Buffs.Length; i++)
                {
                    for (int j = 0; j < attributes.Length; j++)
                    {
                        if (attributes[j].type == _slot.Item.Buffs[i].attributes)
                            attributes[j].value.AddModifier(_slot.Item.Buffs[i]);
                    }
                }
<<<<<<< Updated upstream

                break;
            case InterfaceType.Chest:
=======
                if(_slot.ItemObject.characterDisplay != null)
                {
                    switch (_slot.AllowedItems[0])
                    {                       
                        case ItemType.Weapon:
                            rightHand = Instantiate(_slot.ItemObject.characterDisplay, weaponTransform).transform;
                            break;
                        case ItemType.SecondWeapon:                           
                            switch (_slot.ItemObject.Type)
                            {       
                                case ItemType.Weapon:
                                    leftHand = Instantiate(_slot.ItemObject.characterDisplay, leftHandTransform).transform;
                                    break;
                                case ItemType.SecondWeapon:
                                    leftHand = Instantiate(_slot.ItemObject.characterDisplay, leftWristTransform).transform;
                                    break;
                                default:
                                    break;
                            }
                            break;
                        case ItemType.Armor:
                            armor = boneCombiner.AddLimb(_slot.ItemObject.characterDisplay, _slot.ItemObject.boneName);
                            break;
                        case ItemType.Boots:
                            boots = boneCombiner.AddLimb(_slot.ItemObject.characterDisplay, _slot.ItemObject.boneName);
                            break;
                        case ItemType.Helmet:
                            helmet = boneCombiner.AddLimb(_slot.ItemObject.characterDisplay, _slot.ItemObject.boneName);
                            break;
                    }
                }
>>>>>>> Stashed changes
                break;
            default:
                break;
        }
    }
    private void OnTriggerEnter(Collider other)
    {
        var item = other.GetComponent<GroundItem>();
        if (item)
        {
            Item _item = new Item(item.item);
            if (Inventory.AddItem(_item, 1))
            {
                Destroy(other.gameObject);
            }            
        }
    }
    public void EnterSave(InputAction.CallbackContext callback)
    {
        Inventory.Save();
        Equipment.Save();
    }
    public void LoadSave(InputAction.CallbackContext callback)
    {
        Inventory.Load();
        Equipment.Load();
    }
    private void OnApplicationQuit()
    {
        Inventory.Clear();
        Equipment.Clear();
    }
    public void AttributeModified(Attribute attribute)
    {
        Debug.Log(string.Concat(attribute.type, " was Updated!! Value now is ", attribute.value.ModifiedValue));
    }
}

[System.Serializable]
public class Attribute
{
    [System.NonSerialized]
    public PlayerInventory parent;
    public Attributes type;
    public ModifiableInt value;

    public void SetParent(PlayerInventory _parent)
    {
        parent = _parent;
        value = new ModifiableInt(AttributeModified);

    }
    public void AttributeModified()
    {
        parent.AttributeModified(this);
    }

}
