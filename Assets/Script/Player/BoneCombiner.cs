using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class BoneCombiner
{
    private readonly Dictionary<int, Transform> _rootBoneDictionary = new Dictionary<int, Transform>();
    private readonly Transform[] _boneTransform = new Transform[67];
    private readonly Transform _transform;

    public BoneCombiner(GameObject rootObj)
    {
        _transform = rootObj.transform;
        TraverseHierarchy(_transform);
    }
    public Transform AddLimb(GameObject bonedObj, List<string> boneNames)
    {
        Transform limb = ProcessBonedObject(bonedObj.GetComponentInChildren<SkinnedMeshRenderer>(),boneNames);
        limb.SetParent(_transform);
        return limb;
    }

    private Transform ProcessBonedObject(SkinnedMeshRenderer renderer, List<string> boneNames)
    {
        var bonedObject = new GameObject().transform;
        var meshRenderer = bonedObject.gameObject.AddComponent<SkinnedMeshRenderer>();
        //var bones = renderer.bones;
        for (int i = 0; i < boneNames.Count; i++)
        {
            _boneTransform[i] = _rootBoneDictionary[boneNames[i].GetHashCode()]; 
        }
        meshRenderer.bones = _boneTransform;
        meshRenderer.sharedMesh = renderer.sharedMesh;
        meshRenderer.material = renderer.sharedMaterial;
        return bonedObject;
    }
    private void TraverseHierarchy(IEnumerable transform)
    {
        foreach (Transform child in transform)
        {
            _rootBoneDictionary.Add(child.name.GetHashCode(),child);
            TraverseHierarchy(child);
        }
    }

}
