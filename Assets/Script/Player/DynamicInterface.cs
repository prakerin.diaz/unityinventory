using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class DynamicInterface : UserInterface
{
    public GameObject InventoryPrefab;
    //public int X_START;
    //public int Y_START;
    //public int X_SPACE;
    //public int Y_SPACE;
    //public int NUMBER_OF_COLUMN;
    public override void CreateSlots()
    {
        slotsOnInterface = new Dictionary<GameObject, InventorySlot>();
        for (int i = 0; i < Inventory.Container.Slots.Length; i++)
        {
            var obj = Instantiate(InventoryPrefab, Vector3.zero, Quaternion.identity, transform);
            //obj.GetComponent<RectTransform>().localPosition = GetPos(i);

            AddEvent(obj, EventTriggerType.PointerEnter, delegate { OnEnter(obj); });
            AddEvent(obj, EventTriggerType.PointerExit, delegate { OnExit(obj); });
            AddEvent(obj, EventTriggerType.BeginDrag, delegate { OnDragStart(obj); });
            AddEvent(obj, EventTriggerType.EndDrag, delegate { OnDragEnd(obj); });
            AddEvent(obj, EventTriggerType.Drag, delegate { OnDrag(obj); });

            Inventory.GetSlots[i].slotDisplay = obj;
            slotsOnInterface.Add(obj, Inventory.Container.Slots[i]);
        }
    }
    //private Vector3 GetPos(int pos)
    //{
    //    return new Vector3(X_START + (X_SPACE * (pos % NUMBER_OF_COLUMN)), Y_START + (-Y_SPACE * (pos / NUMBER_OF_COLUMN)), 0f);
    //}

}
