using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Food Object", menuName = "Inventory/Items/Npc")]
public class NpcObjectType : ItemObject
{
    public InventoryObject InventoryNpc;
    private void Awake()
    {
        Type = ItemType.Npc;
    }

    //private void OnTriggerEnter(Collider other)
    //{
    //    var item = other.GetComponent<PlayerInventory>();
    //    if (item)
    //    {
    //        InventoryNpc.Container.Clear();
    //    }
    //}
}
