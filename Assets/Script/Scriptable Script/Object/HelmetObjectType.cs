using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Equipment Object", menuName = "Inventory/Items/Helmet")]
public class HelmetObjectType : ItemObject
{
    private void Awake()
    {
        Type = ItemType.Helmet;
    }
}
