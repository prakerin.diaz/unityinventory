using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Equipment Object", menuName = "Inventory/Items/Weapon")]
public class WeaponObjectType : ItemObject
{
    private void Awake()
    {
        Type = ItemType.Weapon;
    }
}
