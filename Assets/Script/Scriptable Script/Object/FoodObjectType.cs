using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName ="New Food Object", menuName = "Inventory/Items/Food")]
public class FoodObjectType : ItemObject
{
    private void Awake()
    {
        Type = ItemType.Food;
    }

}
