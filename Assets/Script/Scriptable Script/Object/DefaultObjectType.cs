using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName ="New Default Object", menuName = "Inventory/Items/Default")]
public class DefaultObjectType : ItemObject
{
    private void Awake()
    {
        Type = ItemType.Default;
    }

}
