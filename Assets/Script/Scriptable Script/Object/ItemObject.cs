using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class ItemObject : ScriptableObject
{
    public Sprite uiDisplay;
    public bool stackable;
    public ItemType Type;
    [TextArea(15,20)]
    public string Description;
    public Item data = new Item();

    public List<string> boneName = new List<string>();

    public Item CreateItem()
    {
        Item newItem = new Item(this);
        return newItem;
    }
    private void OnValidate()
    {
        boneName.Clear();
        if (characterDisplay == null)
            return;
        if (!characterDisplay.GetComponent<SkinnedMeshRenderer>())
            return;
        var renderer = characterDisplay.GetComponent<SkinnedMeshRenderer>();
        var bones = renderer.bones;

        foreach (var t in bones)
        {
            boneName.Add(t.name);
        }
    }
}
[System.Serializable]
public class Item
{
    public string Name;
    public int Id = -1;
    public ItemBuff[] Buffs;
    public Item()
    {
        Name = "";
        Id = -1;
    }
    public Item(ItemObject item)
    {
        Name = item.name;
        Id = item.data.Id;
        Buffs = new ItemBuff[item.data.Buffs.Length];
        for(int i = 0;i<Buffs.Length; i++)
        {
            Buffs[i] = new ItemBuff(item.data.Buffs[i].min, item.data.Buffs[i].max)
            {
                attributes = item.data.Buffs[i].attributes
            };
        }
    }
}
[System.Serializable]
public class ItemBuff : IModifiers
{
    public Attributes attributes;
    public int value;
    public int min;
    public int max;

    public ItemBuff(int _min, int _max)
    {
        min = _min;
        max = _max;
        GenerateValue();
    }

    public void AddValue(ref int baseValue)
    {
        baseValue += value;
    }

    public void GenerateValue()
    {
        value = UnityEngine.Random.Range(min, max);
    }
}
public enum ItemType
{
    Food,
    Weapon,
    SecondWeapon,
    Armor,
    Boots,
    Helmet,
    Default,
    Npc
}
public enum Attributes
{
    Agility,
    Intellect,
    Stamina,
    Strength
}