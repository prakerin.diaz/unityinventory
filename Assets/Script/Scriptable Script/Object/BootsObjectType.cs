using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Equipment Object", menuName = "Inventory/Items/Boots")]
public class BootsObjectType : ItemObject
{
    private void Awake()
    {
        Type = ItemType.Boots;
    }
}
