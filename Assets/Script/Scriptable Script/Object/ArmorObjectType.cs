using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Equipment Object", menuName = "Inventory/Items/Armor")]
public class ArmorObjectType : ItemObject
{
    private void Awake()
    {
        Type = ItemType.Armor;
    }
}
